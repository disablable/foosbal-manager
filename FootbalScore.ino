// Include stdlib.h lib for use of "dtostrf" function which converts float value to string
#include <stdlib.h>

// Include U8glib.h lib for communication with 128x64 LCD display
#include <U8glib.h>

// Serial communication pins for LCD
#define PIN_CLK  13  // clock
#define PIN_MOSI 11  // master out slave in
#define PIN_CS   12  // chip select

#define MENU_SIZE 5  // fixed number of menu items

// Input pins
const int startSensorPin = 7;
const int goalASensorPin = 2;
const int goalBSensorPin = 3;
const int menuButtonPin = 4;
const int nextButtonPin = 5;
const int speakerPin = 9;

// Initial values
boolean splashScreen = 1;
boolean gameStartState = HIGH;
boolean menuButtonState = HIGH;
boolean menuButtonChangedState = 0;
boolean nextButtonState = HIGH;
boolean nextButtonChangedState = 0;
boolean menuActive = 0; // 0 || 1
int menuItems[] = {0,1,2,3};
boolean menuItemActive = 0;
int menuItemSelected = 0;
int gameState = 2;  // 0 = paused, 1 = playing, 2 = finished
int gameType = 0; // 0 = Time 10min, 1 = Time 20 min, 2 = Reach 10 goals, 3 = Reach 20 goals
int teamAScore = 0;
int teamBScore = 0;
char lastScoredTeam;  // "A" || "B"

// Call U8glib constructor
U8GLIB_ST7920_128X64 u8g(PIN_CLK, PIN_MOSI, PIN_CS, U8G_PIN_NONE);

// Countdown timer vars
unsigned long Watch, _micro, time = micros();
unsigned int Clock = 0, R_clock;
boolean Reset = false, Stop = false, Paused = false;
volatile boolean timeFlag = false;


void setup(void)
{
  // Push buttons are inputs using pullup
  pinMode(menuButtonPin, INPUT_PULLUP);
  pinMode(nextButtonPin, INPUT_PULLUP);

  // Start game sensor
  pinMode(startSensorPin, INPUT);

  // Goal sensor for team A
  pinMode(goalASensorPin, INPUT);
  attachInterrupt(0, goalA, RISING);

  // Goal sensor for team B
  pinMode(goalBSensorPin, INPUT);
  attachInterrupt(1, goalB, RISING);

  // LCD setup
  // Assign default color value
  // This LCD uses u8g.getMode() -> U8G_MODE_BW
  u8g.setColorIndex(1); // pixel on

  // Speaker
  pinMode(speakerPin, OUTPUT);

  soundIntro();
}


void loop(void)
{
  CountDownTimer(); // run the timer

  // If this is a "time" game and time is up, stop the game
  if (gameType == 1 || gameType == 2) {
    if (TimeCheck(0, 0, 0)) {
      if (gameState != 2) {
        soundGameOver();
      }
      gameState = 2;
    }
  }

  // Check if a ball was released (1st time)
  gameStartState = digitalRead(startSensorPin);
  if (gameStartState == HIGH) {
    if (gameState == 0) { // if game is paused
      gameState = 1;
      soundGameStart();
      // If this is a "time" game, resume the timer
      if (gameType == 1 || gameType == 2) {
        ResumeTimer();
      }
    }
  }

  // "Next" button
  nextButtonState = digitalRead(nextButtonPin);
  if (nextButtonState == LOW) {
    nextButtonChangedState = 1;
  }
  if (nextButtonState == HIGH && nextButtonChangedState == 1) {
    // If menu is active, select next menu item
    if(menuActive) {
      menuItemSelected = menuItemSelected + 1;  // Select next menu item

      if(menuItemSelected >= MENU_SIZE) {  // If we reached end of menu items
        menuItemSelected = 0; // .. back to beginning of menu
      }
    }

    // If menu is not active we are in one of the game modes so this button can
    // undo previous goal
    else if(gameState == 0 || gameState == 2) {
      if(lastScoredTeam == 'A' && teamAScore > 0) {
        teamAScore--; // undo goal
        lastScoredTeam = 0;  // clear data about who scored so undo is disabled
        soundUndoGoal();
        // If this was last goal and game was over, return to paused state
        if (gameState == 2) {
          gameState = 0;
        }
      }
      else if(lastScoredTeam == 'B' && teamBScore > 0) {
        teamBScore--;
        lastScoredTeam = 0;  // clear data about who scored so undo is disabled
        soundUndoGoal();
        // If this was last goal and game was over, return to paused state
        if (gameState == 2) {
          gameState = 0;
        }
      }
    }
    nextButtonChangedState = 0;
  }

  // "Menu" button
  menuButtonState = digitalRead(menuButtonPin);
  if (menuButtonState == LOW) {
    menuButtonChangedState = 1;
  }
  if (menuButtonState == HIGH && menuButtonChangedState == 1) {

    // Toggle menu active state (menu on/off)
    menuActive = !menuActive;

    // If menu is opening
    if (menuActive == 1) {

      // If game was in progress (game is not finished)
      if (gameState != 2) {
        // Pause game
        gameState = 0;

        // If game type WAS "Reach XX time"
        // (Here "gameType" is still last game type that was played before opening menu)
        if (gameType == 1 || gameType == 2) {
          // Pause timer
          PauseTimer();
        }
      }

      // Select first menu item
      menuItemSelected = 0;
    }

    // If menu is closing
    else {
      // Set action according to selected menu item

      // "BACK" is selected
      if (menuItemSelected == 0) {
        // If game was in progress (game is not finished)
        if (gameState != 2) {
          // If game WAS "Reach XX time" resume timer
          // (Here "gameType" is still last game type that was played before opening menu because
          // by selecting "Back" menu item we are not assigning new gameType, we continue using the existing one)
//          if (gameType == 1 || gameType == 2) {
//            ResumeTimer();
//          }
        }
      }

      // Some game is selected
      else {
        // Deactivate splash screen
        splashScreen = 0;

        // Map selected game type
        gameType = menuItemSelected; // Game type indexes (1-4) are same as menu item indexes (1-4).

        // Reset score
        teamAScore = 0;
        teamBScore = 0;

        // If selected game is "Reach XX time" set timer to zero and start it
        if (gameType == 1) {
          SetTimer(0,5,0); // 5 minutes
          StartTimer();
          PauseTimer();
        }
        else if (gameType == 2) {
          SetTimer(0,10,00); // 10 minutes
          StartTimer();
          PauseTimer();
        }

        // If game was finished set game status to paused so it can be
        // started with throwing the ball in
        if (gameState == 2) {
          // Pause game
          gameState = 0;
        }
      }
    }

    menuButtonChangedState = 0;
  }

  // Check if a ball was released (2nd time)
  gameStartState = digitalRead(startSensorPin);
  if (gameStartState == HIGH) {
    if (gameState == 0) { // if game is paused
      gameState = 1;
      soundGameStart();
      // If this is a "time" game, resume the timer
      if (gameType == 1 || gameType == 2) {
        ResumeTimer();
      }
    }
  }

  // Output on LCD
  u8g.firstPage();
  do {
    if(menuActive) {
      drawMenu(menuItemSelected); // Show menu
    }
    else {
      if (splashScreen) {
        drawSplash(); // Show splash screen
      }
      else {
        drawScoreBoard(); // Show scoreboard
      }
    }
  } while (u8g.nextPage());
}


void drawSplash()
{
  u8g.setFont(u8g_font_6x13);
  u8g.drawStr(17, 24, "FOOSBALL MANAGER");
  u8g.setFont(u8g_font_6x10);
  u8g.drawStr(23, 50, "by @disablable");
  u8g.drawStr(43, 64, "ver 1.3");
}


void drawMenu(int active)
{
  u8g.setFont(u8g_font_6x10);

  char* selector[] = {"  ", "  ", "  ", "  ", "  "};
  selector[active] = "->";

  u8g.drawStr(50, 7, "MENU");
  // Back
  u8g.drawStr(6, 16, selector[0]);
  u8g.drawStr(20, 16, "..BACK");
  // Game type 0
  u8g.drawStr(6, 28, selector[1]);
  u8g.drawStr(20, 28, "REACH 5 MINUTES");

  // Game type 1
  u8g.drawStr(6, 40, selector[2]);
  u8g.drawStr(20, 40, "REACH 10 MINUTES");

  // Game type 2
  u8g.drawStr(6, 52, selector[3]);
  u8g.drawStr(20, 52, "REACH 5 GOALS");

  // Game type 3
  u8g.drawStr(6, 64, selector[4]);
  u8g.drawStr(20, 64, "REACH 10 GOALS");
}


void drawScoreBoard()
{
//  drawRuler();

  if(gameType == 1 || gameType == 2) {
    drawTimer();
  }
  else {
    drawScoreTitle();
  }

  drawScore();
  drawTeamNames();
  drawMessage();
}


void drawTimer()
{
  u8g.setFont(u8g_font_6x10);
  char timerStr[8];
  int minutes = ShowMinutes();
  int seconds = ShowSeconds();
  sprintf(timerStr, "%02d:%02d", minutes, seconds);
  u8g.drawStr(49, 8, timerStr);
}


void drawScoreTitle()
{
  u8g.setFont(u8g_font_6x10);
  if (gameType == 3) {
    u8g.drawStr(20, 8, "Reach 5 goals");
  }
  if (gameType == 4) {
    u8g.drawStr(20, 8, "Reach 10 goals");
  }
}


void drawScore()
{
  u8g.setFont(u8g_font_fur25);

  char teamBScoreStr[5];
  sprintf(teamBScoreStr, "%d", teamBScore);

  // if Team B score > 9 make smaller offset
  if (teamBScore > 9) {
    u8g.drawStr(20, 48, teamBScoreStr);
  }
  else {
    u8g.drawStr(38, 48, teamBScoreStr);
  }

  u8g.drawStr(59, 44, ":");

  char teamAScoreStr[5];
  sprintf(teamAScoreStr, "%d", teamAScore);
  u8g.drawStr(70, 48, teamAScoreStr);
}


void drawTeamNames()
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawStr(118, 17, "W");
  u8g.drawStr(118, 26, "H");
  u8g.drawStr(118, 35, "I");
  u8g.drawStr(118, 44, "T");
  u8g.drawStr(118, 53, "E");

  u8g.drawStr(5, 21, "B");
  u8g.drawStr(5, 30, "L");
  u8g.drawStr(5, 39, "U");
  u8g.drawStr(5, 48, "E");
}


void drawMessage()
{
  u8g.setFont(u8g_font_6x10);

  if (gameState == 0) {
    u8g.drawStr(14, 64, "Release the ball!");
  }
  else if (gameState == 2) {
    u8g.drawStr(36, 64, "Game over!");
  }
}


void goalA()
{
  goal('A');
}


void goalB()
{
  goal('B');
}


void goal(char team)
{
  if (gameState == 1) {
    // Pause game
    gameState = 0;
    PauseTimer();
    soundGoal();

    if (team == 'A') {
      teamAScore++;
      lastScoredTeam = 'A';
    }
    else if (team == 'B') {
      teamBScore++;
      lastScoredTeam = 'B';
    }

    // If this is "Reach XX goals" game type check if game is over
    if (gameType == 3) {
      // Max 10 goals
      if (teamAScore >= 5 || teamBScore >= 5) {
        gameState = 2;  // Finished
        soundGameOver();
      }
    }
    else if (gameType == 4) {
        // Max 20 goals
      if (teamAScore >= 10 || teamBScore >= 10) {
        gameState = 2;  // Finished
        soundGameOver();
      }
    }
  }
}


void soundIntro()
{
  char notes[] = "CffC";
  int beats[] = {2, 1, 1, 1};
  int length = 4;
  int tempo = 80;

  playSound(notes, beats, length, tempo);
}

void soundGoal()
{
  char notes[] = "a C";
  int beats[] = {2, 1, 8};
  int length = 3;
  int tempo = 80;

  playSound(notes, beats, length, tempo);
}


void soundUndoGoal()
{
  char notes[] = "Ca";
  int beats[] = {2, 1, 6};
  int length = 3;
  int tempo = 80;

  playSound(notes, beats, length, tempo);
}


void soundGameStart()
{
  char notes[] = "faC";
  int beats[] = {1, 1, 1};
  int length = 3;
  int tempo = 80;

  playSound(notes, beats, length, tempo);
}


void soundGameOver()
{
  char notes[] = "faCfaCfaCfaC";
  int beats[] = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
  int length = 12;
  int tempo = 80;

  playSound(notes, beats, length, tempo);
}


/////////////////////////
// Countdown timer lib //
/////////////////////////

boolean CountDownTimer()
{
  static unsigned long duration = 1000000; // 1 second
  timeFlag = false;

  if (!Stop && !Paused) // if not Stopped or Paused, run timer
  {
    // check the time difference and see if 1 second has elapsed
    if ((_micro = micros()) - time > duration )
    {
      Clock--;
      timeFlag = true;

      if (Clock == 0) // check to see if the clock is 0
        Stop = true; // If so, stop the timer

      // Set current time so that we can check against it if 1 second has
      // passed since the last time "Clock" was updated
      time = _micro;
    }
  }
  return !Stop; // return the state of the timer
}

void ResetTimer()
{
  SetTimer(R_clock);
  Stop = false;
}

void StartTimer()
{
  Watch = micros(); // get the initial microseconds at the start of the timer
  Stop = false;
  Paused = false;
}

void StopTimer()
{
  Stop = true;
}

void StopTimerAt(unsigned int hours, unsigned int minutes, unsigned int seconds)
{
  if (TimeCheck(hours, minutes, seconds) )
    Stop = true;
}

void PauseTimer()
{
  Paused = true;
}

void ResumeTimer() // You can resume the timer if you ever stop it.
{
  Paused = false;
}

void SetTimer(unsigned int hours, unsigned int minutes, unsigned int seconds)
{
  // This handles invalid time overflow ie 1(H), 0(M), 120(S) -> 1, 2, 0
  unsigned int _S = (seconds / 60), _M = (minutes / 60);
  if(_S) minutes += _S;
  if(_M) hours += _M;

  Clock = (hours * 3600) + (minutes * 60) + (seconds % 60);
  R_clock = Clock;
  Stop = false;
}

void SetTimer(unsigned int seconds)
{
 // StartTimer(seconds / 3600, (seconds / 3600) / 60, seconds % 60);
 Clock = seconds;
 R_clock = Clock;
 Stop = false;
}

int ShowHours()
{
  return Clock / 3600;
}

int ShowMinutes()
{
  return (Clock / 60) % 60;
}

int ShowSeconds()
{
  return Clock % 60;
}

unsigned long ShowMilliSeconds()
{
  return (_micro - Watch)/ 1000.0;
}

unsigned long ShowMicroSeconds()
{
  return _micro - Watch;
}

boolean TimeHasChanged()
{
  return timeFlag;
}

// output true if timer equals requested time
boolean TimeCheck(unsigned int hours, unsigned int minutes, unsigned int seconds)
{
  return (hours == ShowHours() && minutes == ShowMinutes() && seconds == ShowSeconds());
}


/////////////////
// Play sounds //
/////////////////

void playSound(char notes[], int beats[], int length, int tempo)
{
  // DEMO melody
  // char notes[] = "ccggaagffeeddc "; // a space represents a rest
  // int beats[] = { 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4 };
  // int tempo = 300;

  for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } else {
      playNote(notes[i], beats[i] * tempo);
    }

    // pause between notes
    delay(tempo / 2);
  }
}

void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);
  }
}

void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };

  // play the tone corresponding to the note name
  for (int i = 0; i < 8; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}
